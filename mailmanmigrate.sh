#!/bin/bash -x
# Ldap connection

suffix="dc=example,dc=tld"
ldapbase="o=hosting,"$suffix
dkimdn="ou=opendkim,ou=cpanel,"$suffix
dkimdomains=$(ldapsearch -H ldapi:// -Y EXTERNAL -LLL -s one -b "$dkimdn" "(objectClass=organizationalUnit)" | grep ou: | sed "s|.*: \(.*\)|\1|")

for domain in `su - postgres -c "psql -d mailman -A -t -c \"select mail_host FROM domain;\""`;
do
    if echo ${domain} | grep -q -w "$dkimdomains"; then
        echo "dkim ebtry already exists"
    else
        printf "$domain\n"
        ldapmodify -a -Q -Y EXTERNAL -H ldapi:/// << EOF
dn: ou=$domain,ou=opendkim,ou=cpanel,dc=example,dc=tld
objectClass: organizationalUnit
objectClass: top
ou: $domain
EOF
    fi
done


